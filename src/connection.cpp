#include "tdb_driver.hpp"

using namespace TDb;

void Connection::connect()
{
	sock = sock_create(false);
	sock_open(sock, host, port);
}

void Connection::reconnect()
{
	sock_close(sock);

	connect();
	if(logged) login(username, password);
}

bool Connection::login(const string& username, const string& password)
{
	if(username.empty()) throw invalid_argument("Empty username!");

	// TODO

	char type;
	sock_read(sock, &type, 1);

	bool success;

	if(type == 0) {
		sock_read(sock, (char*) &success, 1);
	} else if(type == 2) {
		// TODO Error
		return false;
	}

	if(success) {
		this->username = username;
		this->password = password;
		logged = true;
	}

	return success;
}

Request Connection::table(const string& name)
{
	return Request(sock, name);
}

/* void Connection::request(const string& str)
{
	// TODO Use a fixed size type
	const size_t size = str.size() + 1;

	try {
		sock_write(sock, (char*) &size, sizeof(size));
		sock_write(sock, str.c_str(), size);
	} catch(const socket_exception& e) {
		if(auto_reconnect) {
			reconnect();

			sock_write(sock, (char*) &size, sizeof(size));
			sock_write(sock, str.c_str(), size);
		} else {
			throw e;
		}
	}
}

void Connection::fetch(Data& data) const
{
	fetch_head(data);
	fetch_body(data);
}

void Connection::fetch_head(Data& data) const
{
	char type;
	sock_read(sock, &type, sizeof(type));

	if(type != 1) {
		// TODO Error
		return;
	}

	// TODO Use a fixed size type
	uint32_t head_size;
	read_count(sock, (char*) &head_size, sizeof(head_size));

	for(size_t i = 0; i < head_size; ++i) {
		string name;
		char c;

		while(true) {
			read_count(sock, &c, sizeof(c));
			if(c == '\0') break;

			name += c;
		}

		Type type;
		read_count(sock, (char*) &type, sizeof(type));

		uint8_t size;
		read_count(sock, (char*) &size, sizeof(size));

		data.columns.emplace_back(name, type, size);
	}
}

void Connection::fetch_body(Data& data) const
{
	while(true) {
		index_t index;
		read_count(sock, (char*) &index, sizeof(index));

		const auto& columns = data.columns;
		vector<void*> values;

		for(size_t j = 0; j < columns.size(); ++j) {
			const auto& size = columns[j].size;

			// TODO Handle CLOBs
			void* val;
			if(!(val = malloc(size))) throw bad_alloc();

			read_count(sock, (char*) val, size);
			values.push_back(val);
		}

		data.rows.emplace_back(index, values);

		if(is_null(data.columns, data.rows.back())) {
			data.rows.pop_back();
			break;
		}
	}
} */
